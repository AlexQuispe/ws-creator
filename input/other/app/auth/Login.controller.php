<?php
defined('APP_PATH') OR die('Access denied');
include_once (APP_PATH.DS.'core'.DS.'Controller.php');
include_once (APP_PATH.DS.'auth'.DS.'Auth.php');

class LoginCtrl extends Controller {
  function login() {
    $obj = $this->get_data();
    if (!(isset($obj->user) && isset($obj->pass))) {
      App::response_precondition_failed();
    }
    $auth = Auth::login($obj->user, $obj->pass);
    if (isset($auth['error'])) {
      App::response_unauthorized($auth['error']);
    }
    App::response_ok($auth);
  }
}
