<?php
defined('APP_PATH') OR die('Access denied');
include_once (APP_PATH.DS.'config'.DS.'config.php');

class Database {
  private static $instanceDatabase;
  private $db_host      = DB_HOST;
  private $db_name      = DB_NAME;
  private $db_user      = DB_USER;
  private $db_password  = DB_PASS;
  private $PDO;

  private function __construct() {
    $dsn = 'mysql:host='.$this->db_host.';dbname='.$this->db_name.';charset=UTF8';
    try {
      $this->PDO = new PDO($dsn, $this->db_user, $this->db_password);
    } catch (Exception $e) {
      App::response_internal_server_error("No es posible conectar con la Base de datos");
    }
  }

  public static function instance() {
    if (!isset(self::$instanceDatabase)) {
      self::$instanceDatabase = new Database;
    }
    return self::$instanceDatabase;
  }

  public function prepare($query) {
    return $this->PDO->prepare($query);
  }

  public function lastInsertId() {
    return $this->PDO->lastInsertId();
  }

  public function __destruct() {
    unset($this);
  }
}
?>
