<?php
defined('APP_PATH') OR die('Access denied');
include_once (APP_PATH.DS.'core'.DS.'Controller.php');
include_once (APP_PATH.DS.'actions'.DS.'Accion.php');
include_once (APP_PATH.DS.'models'.DS.'Certificado.php');
include_once (APP_PATH.DS.'models'.DS.'Documento.php');
include_once (APP_PATH.DS.'models'.DS.'Cargo.php');

class AccionCtrl extends Controller {

    function accion_crear_certificado() {
      $auth = Auth::verify_access([ROL_POSTULANTE]);
      if (isset($auth['error'])) {
        App::response_unauthorized($auth['error']);
      }
      $obj = $this->get_data();
      if(!(isset($obj->titulo) && isset($obj->tipo_presencia) && isset($obj->resolucion) &&
      isset($obj->categoria) && isset($obj->gestion) && isset($obj->periodo))) {
        App::response_precondition_failed();
      }
      /*if (Certificado::existByAll($obj->titulo, $obj->tipo_presencia, $obj->resolucion, $obj->categoria, $obj->gestion, $obj->periodo, $auth->id_usuario)) {
        App::response_unprocessable_entity('Ya existe un registro con esos datos.');
      }*/

      $id_usuario = $auth['id_usuario'];
      $titulo = $obj->titulo;
      $tipo_presencia = $obj->tipo_presencia;
      $resolucion = $obj->resolucion;
      $categoria = $obj->categoria;
      $estado = "Registrado";
      $puntaje = 0;
      $gestion = $obj->gestion;
      $periodo = $obj->periodo;

      $data = Certificado::insert($titulo, $tipo_presencia, $resolucion, $categoria, $estado, $puntaje, $gestion, $periodo, $id_usuario);
      $response_data = array();
      $response_data['id_certificado'] = $data['id_certificado'];
      $response_data['puntaje'] = $puntaje;
      App::response_ok($response_data);
    }

    function accion_actualizar_certificado() {
      $auth = Auth::verify_access([ROL_KARDEX, ROL_TRIBUNAL, ROL_POSTULANTE]);
      if (isset($auth['error'])) {
        App::response_unauthorized($auth['error']);
      }
      $obj = $this->get_data();
      if(!isset($_GET['id'])) {
        App::response_precondition_failed('No colocaste el ID');
      }
      $data = Certificado::getById($_GET['id']);
      if (!$data) {
        App::response_conflict();
      }
      if(!(isset($obj->titulo) && isset($obj->tipo_presencia) && isset($obj->resolucion) &&
      isset($obj->categoria) && isset($obj->estado) && isset($obj->gestion) && isset($obj->periodo))) {
        App::response_precondition_failed();
      }

      $id_certificado = $_GET['id'];
      $id_usuario = $auth['id_usuario'];
      $titulo = $obj->titulo;
      $tipo_presencia = $obj->tipo_presencia;
      $resolucion = $obj->resolucion;
      $categoria = $obj->categoria;
      $estado = $obj->estado;
      $puntaje = 0;
      $gestion = $obj->gestion;
      $periodo = $obj->periodo;

      $data = Certificado::update($id_certificado, $titulo, $tipo_presencia, $resolucion, $categoria, $estado, $puntaje, $gestion, $periodo, $id_usuario);
      App::response_ok();
    }

    function accion_crear_documento() {
      $auth = Auth::verify_access([ROL_KARDEX, ROL_TRIBUNAL, ROL_POSTULANTE]);
      if (isset($auth['error'])) {
        App::response_unauthorized($auth['error']);
      }
      $obj = $this->get_data();
      if(!(isset($obj->nombre) && isset($obj->id_convocatoria))) {
        App::response_precondition_failed();
      }
      if (Documento::existByAll($obj->nombre)) {
        App::response_unprocessable_entity('Ya existe un registro con esos datos.');
      }
      $data = Convocatoria::getById($obj->id_convocatoria);
      if (!$data) {
        App::response_conflict('No existe el registro convocatoria');
      }

      $nombre = $obj->nombre;
      $id_convocatoria = $obj->id_convocatoria;

      $data = Documento::insert($nombre);
      $id_documento = $data['id_documento'];
      $data = ConvocatoriaDocumento::insert($id_convocatoria, $id_documento);
      App::response_ok();
    }

    function accion_crear_cargo() {
      $auth = Auth::verify_access([ROL_KARDEX, ROL_TRIBUNAL, ROL_POSTULANTE]);
      if (isset($auth['error'])) {
        App::response_unauthorized($auth['error']);
      }
      $obj = $this->get_data();
      if(!(isset($obj->nombre) && isset($obj->materia) && isset($obj->sigla) && isset($obj->id_convocatoria))) {
        App::response_precondition_failed();
      }
      if (Cargo::existByAll($obj->nombre, $obj->materia, $obj->sigla)) {
        App::response_unprocessable_entity('Ya existe un registro con esos datos.');
      }
      $data = Convocatoria::getById($obj->id_convocatoria);
      if (!$data) {
        App::response_conflict('No existe el registro convocatoria');
      }

      $nombre = $obj->nombre;
      $materia = $obj->materia;
      $sigla = $obj->sigla;
      $id_convocatoria = $obj->id_convocatoria;

      $data = Cargo::insert($nombre, $materia, $sigla);
      $id_cargo = $data['id_cargo'];
      $data = ConvocatoriaCargo::insert($id_convocatoria, $id_cargo);
      App::response_ok();
    }

    function accion_publicar() {
      $auth = Auth::verify_access([ROL_KARDEX, ROL_TRIBUNAL, ROL_POSTULANTE]);
      if (isset($auth['error'])) {
        App::response_unauthorized($auth['error']);
      }
      $obj = $this->get_data();
      if(!(isset($obj->id_convocatoria))) {
        App::response_precondition_failed();
      }
      $data = Convocatoria::getById($obj->id_convocatoria);
      if (!$data) {
        App::response_conflict('No existe el registro convocatoria');
      }

      $fecha_publicacion = date('Y-m-d H:i:s');
      $estado = "Publicado";

      $data = Accion::publicar_convocatoria($obj->id_convocatoria, $fecha_publicacion, $estado);
      App::response_ok($data);
    }

}
