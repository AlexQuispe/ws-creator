<?php
defined('APP_PATH') OR die('Access denied');
include_once (APP_PATH.DS.'core'.DS.'Database.php');

include_once (APP_PATH.DS.'models'.DS.'Convocatoria.php');

class Accion extends Database {

  public static function publicar_convocatoria($id_convocatoria, $fecha_publicacion, $estado) {
    $query = 'UPDATE convocatoria SET fecha_publicacion = ?, estado = ? WHERE id_convocatoria = ?';
    $PDO = DATABASE::instance();
    $PDO_stmt = $PDO->prepare($query);
    $PDO_stmt->bindParam(1, $fecha_publicacion, PDO::PARAM_STR);
    $PDO_stmt->bindParam(2, $estado, PDO::PARAM_STR);
    $PDO_stmt->bindParam(3, $id_convocatoria, PDO::PARAM_INT);
    $PDO_stmt->execute();
    unset($PDO_stmt);


    //App::response();
  }
}
