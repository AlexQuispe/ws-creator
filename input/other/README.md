# CV Backend

Servicio backend para gestionar curriculums vitae digitales.

#### NOTA:
El password para todos los roles de usuario es: 123.

#### códigos de respuesta:

- `200` **Ok**. La petición del navegador se ha completado con éxito.
- `204` **No Content**. La petición se ha completado con éxito pero su respuesta no tiene ningún contenido.
- `400` **Bad Request**. El servidor no es capaz de entender la petición del navegador porque su sintaxis no es correcta.
- `401` **Unauthorized**. El recurso solicitado por el navegador requiere de autenticación.
- `404` **Not Found**. El servidor no puede encontrar el recurso solicitado por el navegador.
- `405` **Method Not Allowed**. El navegador ha utilizado un método (GET, POST, etc.) no permitido por el servidor para obtener ese recurso.
- `412` **Precondition Failed**. El servidor no es capaz de cumplir con algunas de las condiciones impuestas por el navegador en su petición.
- `422` **Unprocessable Entity**. La petición del navegador tiene el formato correcto, pero sus contenidos tienen algún error semántico que impide al servidor responder.
- `500` **Internal Server Error**. Se ha producido un error interno.

#### Informe de Avance

##### 1. Servicio de autenticación
- [x] login. _Tested_

##### 2. Modelos y Controladores
- [x] rol **[Basic]**.  _Tested_
- [x] usuario.  _Tested_
- [x] convocatoria **[Basic]**.  _Tested_
- [x] cargo **[Basic]**.  _Tested_
- [x] tribunal.  _Tested_
- [x] convocatoria_cargo.  _Tested_
- [x] certificado.  _Tested_
- [x] certificado_tribunal.  _Tested_
- [x] certificado_convocatoria.  _Tested_
- [x] documento **[Basic]**.  _Tested_
- [x] convocatoria_documento.  _Tested_
- [x] postulante.  _Tested_
- [x] documento_presentado.  _Tested_
- [x] log **[Basic]**.  _Tested_

#### 3. Reportes
- [x] reporte_certificado_categoria (devuelve todos los certificados por categoría).
- [x] reporte_convocatoria_cargo (devuelve todos los cargos asignados a un convocatoria dado el id_convocatoria).
- [x] reporte_datos_personales (develve todos los datos del usuario).
- [x] reporte_usuario_rol (develve los datos del usuario actual).

#### 3. Acciones
- [ ] accion_guardar_certificado (guarda un certificado pasándole solo algunos datos y devuelve el puntaje).

#### Sugerencias
2. Verificar la eliminación de tablas cuyas PK están en otras tablas.

#### Referencias

Los códigos de estado de HTTP
http://librosweb.es/tutorial/los-codigos-de-estado-de-http/

PHP Authorization with JWT (JSON Web Tokens)
https://www.sitepoint.com/php-authorization-jwt-json-web-tokens/

Consumiendo webservices SOAP desde PHP
http://blog.osusnet.com/2008/08/06/consumiendo-webservices-soap-desde-php/
