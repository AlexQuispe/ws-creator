<?php
case 'login':
$controller = new LoginCtrl;
switch ($method) {
  case 'POST':    $controller->login(); break;
  case 'OPTIONS': break;
  default:        App::response_method_not_allowed(); break;
}
break;
case 'reporte_datos_personales':
$controller = new ReporteCtrl;
switch ($method) {
  case 'POST':     $controller->reporte_datos_personales(); break;
  case 'OPTIONS': break;
  default:        App::response_method_not_allowed(); break;
}
break;
case 'reporte_datos_personales_tribunal':
$controller = new ReporteCtrl;
switch ($method) {
  case 'POST':     $controller->reporte_datos_personales_tribunal(); break;
  case 'OPTIONS': break;
  default:        App::response_method_not_allowed(); break;
}
break;
case 'reporte_tribunal_convocatoria':
$controller = new ReporteCtrl;
switch ($method) {
  case 'POST':     $controller->reporte_tribunal_convocatoria(); break;
  case 'OPTIONS': break;
  default:        App::response_method_not_allowed(); break;
}
break;
case 'accion_certificado':
$controller = new AccionCtrl;
switch ($method) {
  case 'POST':    $controller->accion_crear_certificado(); break;
  case 'PUT':     $controller->accion_actualizar_certificado(); break;
  case 'OPTIONS': break;
  default:        App::response_method_not_allowed(); break;
}
break;
case 'accion_crear_documento':
$controller = new AccionCtrl;
switch ($method) {
  case 'POST':    $controller->accion_crear_documento(); break;
  case 'OPTIONS': break;
  default:        App::response_method_not_allowed(); break;
}
break;
case 'accion_crear_cargo':
$controller = new AccionCtrl;
switch ($method) {
  case 'POST':    $controller->accion_crear_cargo(); break;
  case 'OPTIONS': break;
  default:        App::response_method_not_allowed(); break;
}
break;
case 'accion_publicar':
$controller = new AccionCtrl;
switch ($method) {
  case 'POST':    $controller->accion_publicar(); break;
  case 'OPTIONS': break;
  default:        App::response_method_not_allowed(); break;
}
break;
