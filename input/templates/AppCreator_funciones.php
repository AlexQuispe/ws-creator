<?php
private static function response($code, $status, $data) {
  header('Content-Type: application/JSON; charset=utf-8');
  http_response_code($code);
  $response = array ();
  $response['code'] = $code;
  $response['status'] = $status;
  $response['data'] = $data;
  echo json_encode($response);
  exit;
}
public static function response_ok($data=array('success'=>'Perfecto.')) {
  App::response(200, 'Ok', $data);
}
public static function response_bad_request($detail='No entiendo lo que quieres.') {
  App::response(400, 'Bad Request', array('error'=>$detail));
}
public static function response_unauthorized($detail='Acceso denegado.') {
  App::response(401, 'Unauthorized', array('error'=>$detail));
  //App::response(401, 'Unauthorized', array('error'=>'Acceso denegado.'));
}
public static function response_not_found($detail='Ese recurso no existe.') {
  App::response(404, 'Not Found', array('error'=>$detail));
}
public static function response_method_not_allowed($detail='No puedes usar ese método.') {
  App::response(405, 'Method Not Allowed', array('error'=>$detail));
}
public static function response_conflict($detail='No existe el registro.') {
  App::response(409, 'Conflict', array('error'=>$detail));
}
public static function response_precondition_failed($detail='¿Estás enviando todos los datos?.') {
  App::response(412, 'Precondition Failed', array('error'=>$detail));
}
public static function response_unprocessable_entity($detail='Algunos datos NO son válidos.') {
  App::response(422, 'Unprocessable Entity', array('error'=>$detail));
}
public static function response_internal_server_error($detail='Ups, es culpa nuestra, disculpe las molestias.') {
  App::response(500, 'Internal Server Error', array('error'=>$detail));
}
