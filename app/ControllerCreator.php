<?php
defined('PROJECT_PATH') OR die('Access denied');

class ControllerCreator {

  private $content;

  private $model;
  private $class_name;

  private $access_get;                // ROL_ADMIN, ROL_KARDEX, ROL_TRIBUNAL
  private $access_post;               // ROL_ADMIN, ROL_KARDEX, ROL_TRIBUNAL
  private $access_put;                // ROL_ADMIN, ROL_KARDEX, ROL_TRIBUNAL
  private $access_delete;             // ROL_ADMIN, ROL_KARDEX, ROL_TRIBUNAL
  private $attributes_isset;          // isset($obj->col1) && isset($obj->col2) && isset($obj->key1)
  private $attributes;                // $obj->col1, $obj->col2, $obj->key1

  private $variables_unique;          // $col1, $col2, $key1

  function init() {
    $this->content = "<?php\n";
  }

  function setModel($filename) {
    $modelJSON = file_get_contents($filename);
    $model = json_decode($modelJSON, true);
    $this->model = $model;

    $this->class_name = $this->camelCase($model['table_name']);
    $this->access_get = $model['access_get'];
    $this->access_post = $model['access_post'];
    $this->access_put = $model['access_put'];
    $this->access_delete = $model['access_delete'];

    $attributes_isset = "";
    $attributes = "";

    $variables_unique = "";

    foreach ($model['atributes'] as $atribute) {
      $name = $atribute['name'];
      $attributes_isset .= "isset(\$obj->$name) && ";
      $attributes .= "\$obj->$name, ";

      if ($atribute['unique']) {
        $variables_unique .= "\$obj->$name, ";
      }
    }

    foreach ($model['foreign_keys'] as $atribute) {
      $name = $atribute['name'];
      $attributes_isset .= "isset(\$obj->$name) && ";
      $attributes .= "\$obj->$name, ";

      if ($atribute['unique']) {
        $variables_unique .= "\$obj->$name, ";
      }
    }

    $this->attributes_isset = substr($attributes_isset, 0, strlen($attributes_isset) - 4);
    $this->attributes = substr($attributes, 0, strlen($attributes) - 2);
    $this->variables_unique = substr($variables_unique, 0, strlen($variables_unique) - 2);

    $this->add_header();
    $this->add_class_start();
    $this->add_function_get();
    $this->add_function_post();
    $this->add_function_put();
    $this->add_function_delete();
  }

  function finish() {
    $this->add_class_end();
  }

  function getContent() {
    return $this->content;
  }

  function get_className() {
    return $this->class_name;
  }

  function camelCase($str)
  {
    $str = str_replace("_", " ", $str);
    $str = ucwords($str);
    $str = str_replace(" ", "", $str);
    return $str;
  }

  function add_header() {
    $txt = "defined('APP_PATH') OR die('Access denied');\n";
    $txt .= "include_once (APP_PATH.DS.'core'.DS.'Controller.php');\n";
    $txt .= "include_once (APP_PATH.DS.'models'.DS.'$this->class_name.php');\n";
    foreach ($this->model['foreign_keys'] as $foreign_key) {
      $filename_model = $this->camelCase($foreign_key['table_name']).".php";
      $txt .= "include_once (APP_PATH.DS.'models'.DS.'$filename_model');\n";
    }
    $txt .= "\n";
    $this->content .= $txt;
  }

  function add_class_start() {
    $classnameCtrl = $this->class_name."Ctrl";
    $txt = "class $classnameCtrl extends Controller {\n";
    $txt .= "\n";
    $this->content .= $txt;
  }

  function add_class_end() {
    $txt = "}\n";
    $this->content .= $txt;
  }

  function add_function_get() {
    $txt = "  function get() {\n";
    $txt .= "    \$auth = Auth::verify_access([$this->access_get]);\n";
    $txt .= "    if (isset(\$auth['error'])) {\n";
    $txt .= "      App::response_unauthorized(\$auth['error']);\n";
    $txt .= "    }\n";
    $txt .= "    if(!isset(\$_GET['id'])) {\n";
    $txt .= "      \$data = $this->class_name::getAll();\n";
    $txt .= "      App::response_ok(\$data);\n";
    $txt .= "    }\n";
    $txt .= "    \$data = $this->class_name::getById(\$_GET['id']);\n";
    $txt .= "    if (!\$data) {\n";
    $txt .= "      App::response_conflict();\n";
    $txt .= "    }\n";
    $txt .= "    App::response_ok(\$data);\n";
    $txt .= "  }\n";
    $txt .= "\n";
    $this->content .= $txt;
  }

  function add_function_post() {
    $txt = "  function post() {\n";
    $txt .= "    \$auth = Auth::verify_access([$this->access_post]);\n";
    $txt .= "    if (isset(\$auth['error'])) {\n";
    $txt .= "      App::response_unauthorized(\$auth['error']);\n";
    $txt .= "    }\n";
    $txt .= "    \$obj = \$this->get_data();\n";
    $txt .= "    if(!($this->attributes_isset)) {\n";
    $txt .= "      App::response_precondition_failed();\n";
    $txt .= "    }\n";

    if ($this->variables_unique != "") {
      $txt .= "    if ($this->class_name::existByAll($this->variables_unique)) {\n";
      $txt .= "      App::response_unprocessable_entity('Ya existe un registro con esos datos.');\n";
      $txt .= "    }\n";
    }

    foreach ($this->model['foreign_keys'] as $foreign_key) {
      $table_name = $foreign_key['table_name'];
      $class_name = $this->camelCase($table_name);
      $id_foreign = $foreign_key['name'];

      $txt .= "    if (!$class_name::getById(\$obj->$id_foreign)) {\n";
      $txt .= "      App::response_conflict('No existe el registro $table_name.');\n";
      $txt .= "    }\n";
    }

    $txt .= "    \$data = $this->class_name::insert($this->attributes);\n";
    $txt .= "    App::response_ok(\$data);\n";
    $txt .= "  }\n";
    $txt .= "\n";
    $this->content .= $txt;
  }

  function add_function_put() {
    $txt = "  function put() {\n";
    $txt .= "    \$auth = Auth::verify_access([".$this->access_put."]);\n";
    $txt .= "    if (isset(\$auth['error'])) {\n";
    $txt .= "      App::response_unauthorized(\$auth['error']);\n";
    $txt .= "    }\n";
    $txt .= "    \$obj = \$this->get_data();\n";

    $txt .= "    if(!isset(\$_GET['id'])) {\n";
    $txt .= "      App::response_precondition_failed('No colocaste el ID');\n";
    $txt .= "    }\n";
    $txt .= "    \$data = $this->class_name::getById(\$_GET['id']);\n";
    $txt .= "    if (!\$data) {\n";
    $txt .= "      App::response_conflict();\n";
    $txt .= "    }\n";

    $txt .= "    if(!($this->attributes_isset)) {\n";
    $txt .= "      App::response_precondition_failed();\n";
    $txt .= "    }\n";

    foreach ($this->model['foreign_keys'] as $foreign_key) {
      $table_name = $foreign_key['table_name'];
      $class_name = $this->camelCase($table_name);
      $id_foreign = $foreign_key['name'];

      $txt .= "    if (!$class_name::getById(\$obj->$id_foreign)) {\n";
      $txt .= "      App::response_conflict('No existe el registro $table_name.');\n";
      $txt .= "    }\n";
    }

    $txt .= "    $this->class_name::update(\$_GET['id'], $this->attributes);\n";
    $txt .= "    App::response_ok();\n";
    $txt .= "  }\n";
    $txt .= "\n";
    $this->content .= $txt;
  }

  function add_function_delete() {
    $txt = " function delete() {\n";
    $txt .= "    \$auth = Auth::verify_access([".$this->access_delete."]);\n";
    $txt .= "    if (isset(\$auth['error'])) {\n";
    $txt .= "      App::response_unauthorized(\$auth['error']);\n";
    $txt .= "    }\n";
    $txt .= "    \$obj = \$this->get_data();\n";

    $txt .= "    if(!isset(\$_GET['id'])) {\n";
    $txt .= "      App::response_precondition_failed('No colocaste el ID');\n";
    $txt .= "    }\n";
    $txt .= "    \$data = $this->class_name::getById(\$_GET['id']);\n";
    $txt .= "    if (!\$data) {\n";
    $txt .= "      App::response_conflict();\n";
    $txt .= "    }\n";

    $txt .= "    \$data = $this->class_name::delete(\$_GET['id']);\n";
    $txt .= "    App::response_ok();\n";
    $txt .= "  }\n";
    $txt .= "\n";
    $this->content .= $txt;
  }
}
