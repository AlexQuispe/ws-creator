# WS-CREATOR

WS-CREATOR es un pequeño servicio que genera la estructura básica de un Servicio Web basado en PHP, además de generar su documentación.

### Funciones:

- Crear ENDPOINTS para cualquier tabla en general (GET, POST PUT, DELETE), a partir de un modelo descrito en un archivo JSON.
- crear ENDPOINTS para reportes, igualmente descrito e un archivo JSON.
- Genera la documentación de os ENDPOINTS, a partir de los modelos descritos anteriormente.
- Incluye un sistema de LOGIN basado en JWT para acceder a los recursos del API generada.
- Siempre es posible crear ENDPOINTS de forma manual, incluyendo su documentación.

### Tecnologías utilizadas

- **PHP (Hypertext Preprocessor)**, lenguaje de código abierto utilizado para desarrollar sitios web.
- **JSON (JavaScript Object Notation)**, formato de texto ligero para el intercambio de datos.
- **JWT (JSON Web Token)**, librería escrita en PHP que permite crear tokens de acceso.

### Instalación

Para instalar el proyecto ver el archivo
 [INSTALL.md](https://gitlab.com/aquispe/ws-creator/blob/master/INSTALL.md).

### API DOC - Demo

Un ejemplo de la documentación que se genera se encuentra disponible en el siguiente enlace: http://angular121.hol.es/cv/api/

### Referencias

PHP-JWT
https://github.com/firebase/php-jwt

Los códigos de estado de HTTP
http://librosweb.es/tutorial/los-codigos-de-estado-de-http/

PHP Authorization with JWT (JSON Web Tokens)
https://www.sitepoint.com/php-authorization-jwt-json-web-tokens/
